﻿using Eco.Gameplay.Items;
using Eco.Mods.TechTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Eco.Gameplay.Players;
using Eco.Shared.Utils;

namespace FoodTracker
{
    class Program
    {
        private static float SkillGainMultiplier = 1f;
        private static float BaseSkillGainRate = 12f;
        private static List<FoodItem> registredTypes;

        private static Nutrients Nutrients;

        private static List<FoodCombinaison> combinaisons = new List<FoodCombinaison>();


        static void Main(string[] args)
        {
            Dictionary<FoodItem, int> startStomach = getCurrentStomach();
            float currentScore = getSkill(startStomach);

            Dictionary<Dictionary<FoodItem, int>,float> results = new Dictionary<Dictionary<FoodItem, int>,float>();


            var list = new List<int>();
            list.AddRange(Enumerable.Range(0, getItems().Count));



            var res = GetKCombs(list, 3);

            res.ForEach(i =>
             {
                 List<int> combinaison = i.ToList();

                 FoodItem item1 = getItems()[combinaison[0]];
                 FoodItem item2 = getItems()[combinaison[1]];
                 FoodItem item3 = getItems()[combinaison[2]];

                 if (item1 != null)
                 {
                     FoodCombinaison newCOmb = new FoodCombinaison(item1, item2, item3);
                     combinaisons.Add(newCOmb);
                 }


             });

            int countRest = combinaisons.Count();
            int current = 0;

            Console.WriteLine(combinaisons.Count() + " combinations founded ");

            Console.ReadLine();
            Console.WriteLine("Started calc");

            combinaisons.ForEach(c =>
            {
                current++;

                int percent = current * 100 / countRest;
                if(percent%10==0)
                    Console.WriteLine(percent);

                var combinaison = testCombinaison(c.item1, c.item2, c.item3, currentScore, startStomach);
                if(combinaison.Key!=null)
                    results.Add(combinaison.Key, combinaison.Value);


            });



            //FINAL calcul ===========

            var finalResults = results.OrderByDescending(d => d.Value).ToDictionary(t => t.Key, t => t.Value);

            Console.WriteLine("Current score is : " + currentScore);

            foreach (var keyValuePair in finalResults)
            {
                Console.ReadLine();
                Console.Write("Results: " + keyValuePair.Value + " ==> ");

                foreach(var d in keyValuePair.Key)
                {
                    if(d.Value!=0)
                        Console.Write(d.Key.DisplayName + " X" + d.Value + "   ");
                }
                Console.WriteLine();
            }


            Console.ReadLine();

        }


        public static KeyValuePair<Dictionary<FoodItem, int>,float> testCombinaison(FoodItem item1, FoodItem item2, FoodItem item3, float scoreMin, Dictionary<FoodItem, int> startStomach)
        {//take the top
            int iterate = 0;

            if (item1 == null)
                throw new Exception("Item 1 can't be null");

            Dictionary<Dictionary<FoodItem, int>,float> goodResults = new Dictionary<Dictionary<FoodItem, int>,float>();

            for (int x = 1; x < 2; x++)
            {
                for (int y = 0; y < 2; y++)
                {
                    for (int z = 0; z < 2; z++)
                    {
                        iterate++;

                       // Console.WriteLine("Iterate: "+iterate);
                        Dictionary<FoodItem, int> currentDictionary = new Dictionary<FoodItem, int>(startStomach);

                        currentDictionary.Add(item1, x);
                        if (y > 0 && item2 != null) currentDictionary.Add(item2, y);
                        if (z > 0 && item3 != null) currentDictionary.Add(item3, z);

                        if (hasMaxCalorie(currentDictionary))
                            continue;

                        float score = getSkill(currentDictionary);

                        if (score > scoreMin)
                            goodResults.Add(new Dictionary<FoodItem, int>()
                            {
                                {item1,x },
                                {item2,y },
                                {item3,z },

                            }, score);


                    }
                }
            }

            if (goodResults.Count <= 0)
                return new KeyValuePair<Dictionary<FoodItem, int>, float>(null,0);

            return goodResults.OrderByDescending(t => t.Value).First();
        }


        static IEnumerable<IEnumerable<T>> GetKCombs<T>(IEnumerable<T> list, int length) where T : IComparable
        {
            if (length == 1) return list.Select(t => new T[] { t });

            return GetKCombs(list, length - 1)
                .SelectMany(t => list.Where(o => o.CompareTo(t.Last()) > 0),
                    (t1, t2) => t1.Concat(new T[] { t2 }));
        }


        public static bool alreadyRegistred(List<FoodCombinaison> combinaisons, FoodItem item1, FoodItem item2, FoodItem item3 )
        {
            List<FoodItem> itemsAlreadyCheck = new List<FoodItem>(){item1,item2,item3};

            return combinaisons.Any(c => itemsAlreadyCheck.All(f => f==c.item1 || f ==c.item2 || f == c.item3 ));
        }



        public static Dictionary<FoodItem, int> getCurrentStomach()
        {

            Dictionary<FoodItem, int> stomach = new Dictionary<FoodItem, int>();
            stomach.Add(new RawMeatItem(), 1);
            stomach.Add(new CharredBeetItem(), 1);
            stomach.Add(new WheatPorridgeItem(), 1);

            return stomach;
        }

        public static bool hasMaxCalorie(Dictionary<FoodItem, int> stomach)
        {
            float totalCal = 0;

            foreach (var pair in stomach)
            {
                totalCal += pair.Key.Calories;

                if (totalCal > 3000)
                    return true;
            }

            return false;
        }

        public static float getSkill(Dictionary<FoodItem, int> stomach)
        {


            Nutrients nutrientAvg = default(Nutrients);
            float totalCal = 0;

            foreach (var pair in stomach)
            {
                if (pair.Key.Calories > 0)
                {
                    for (int x = 0; x < pair.Value; x++)
                    {
                        totalCal += pair.Key.Calories;
                        nutrientAvg += pair.Key.Nutrition * pair.Key.Calories;
                    }
                }
            }
            if (totalCal > 0)
                nutrientAvg *= 1.0f / totalCal;

            Nutrients = nutrientAvg;

            float BalanceBonus = Nutrients.Values.Max() > 0 ? (Nutrients.Values.Sum() / (Nutrients.Values.Max() * 4)) * 2 : 0;

            float NutrientSkillRate = ((Nutrients.NutrientTotal * BalanceBonus) + BaseSkillGainRate) * SkillGainMultiplier;

            return NutrientSkillRate;
        }


        public static List<FoodItem> getItems()
        {
            if (registredTypes == null)
            {

                var types = AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(s => s.GetTypes())
                    .Where(p => typeof(FoodItem).IsAssignableFrom(p) && !p.IsAbstract)
                    .Select(t => (FoodItem)Activator.CreateInstance(t))
                    .Where(food => food.Calories > 0 && food.GetType()!=typeof(EcoylentItem))
                    .ToList();



                types.Add(null);
                types.Reverse();
                registredTypes = types;


            }

            return registredTypes;
        }
    }
}
