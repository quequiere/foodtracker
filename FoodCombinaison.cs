﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eco.Gameplay.Items;

namespace FoodTracker
{
    public class FoodCombinaison
    {
        public FoodItem item1;
        public FoodItem item2;
        public FoodItem item3;

        public FoodCombinaison(FoodItem item1, FoodItem item2, FoodItem item3)
        {
            this.item1 = item1;
            this.item2 = item2;
            this.item3 = item3;
        }

        public bool containsItemFoodItem (FoodItem item)
        {
            return (item1 == item || item2 == item || item3 == item);
        }

        public List<FoodItem> getItems()
        {
            return new List<FoodItem>()
            {
                item1,item2,item3
            };
        }

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }

            FoodCombinaison comp = obj as FoodCombinaison;
            return this.getItems().All(i => comp.containsItemFoodItem(i));

        }

        public override string ToString()
        {
            return $"{item1} / {item2} / {item3}";
        }
    }
}
